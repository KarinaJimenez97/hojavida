package com.example.deber1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ReferenciasActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referencias2);
    }
    public void ingresaProfecion(View view) {
        Intent intent = new Intent(this, Profesional.class);
        startActivity(intent);
    }
}